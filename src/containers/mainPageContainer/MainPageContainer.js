import './MainPageContainer.scss'
export default function MainPageContaier(props) {
    return (
        <div className='container'>
            <div className='sideElement'>
                {props.sideElement}   {/* generic sidebar place for component*/}
            </div>
            <div className='mainElement'>
                {props.mainElement}{/* generic main page place for component */}
            </div>
        </div>
    );
}