import { useEffect, useState } from 'react';
import './InsertCommentPopUp.scss';
import axios from "../../third-party/axios";
import Loading from '../../components/loading/Loading';
import swal from 'sweetalert';


export default function InsertCommentPopUp(props) {

  //-----states
  const [message, setMessage] = useState({ email: '', name: '', comment: '', postId: '' }); //the comment information.
  const [isLoading, setLoading] = useState(false); //is the http still procceced

  let cleanAfterUnMount = [];
  //clean open pipes when component unmount.
  useEffect(() => {

  }, cleanAfterUnMount)

  //the function send post request with the from data.
  function insetComment(event) {
    setLoading(true);
    cleanAfterUnMount.push(axios.post('https://test.steps.me/test/testAssignComment', message).then((res) => {
      setLoading(false);
      //popup that tells the user wherever the request successed or failed.
      swal({
        title: "success!",
        text: 'your comment delivered successfuly',
        icon: "success",
        button: "ok",
      });
      //if request failed
    }, (rej) => {
      //popup that tells the user wherever the request successed or failed.
      swal({
        title: "ERROR!",
        text: rej.toString(),
        icon: "warning",
        button: "ok",
      });
    }));
    props.close(); //close 'insertComment' popup.
    event.preventDefault(); //avoiding refresh.
  };
  //generic function that updates the relevante field the user have just updated.
  function handleInputChange(event) {
    const target = event.target;
    const value = target.value; //get the field value
    const name = target.name; //name of the field

    //update the relevante field without overide the other fields.
    setMessage({
      ...message,
      [name]: value
    });
  }

  return (
    <div className='insertPopUp'>
      {isLoading ? < Loading > </Loading> : null}
      <form action="#" id="contact" onSubmit={insetComment}>
        <div className='title'>
          write your comment
        </div>

        <fieldset name='name'>
          <legend>name</legend>
          <input name="name" placeholder="Your name" type="text" onChange={handleInputChange} required>
          </input>
        </fieldset>

        <fieldset>
          <legend>email</legend>
          <input name="email" placeholder="Your Email Address" type="email" onChange={handleInputChange} required >
          </input>
        </fieldset>

        <fieldset>
          <legend>comment</legend>
          <textarea name='comment' placeholder="Type your Message Here...." onChange={handleInputChange} required></textarea>
        </fieldset>

        <fieldset>
          <button name="submit" type="submit" id="contact-submit">Submit</button>
        </fieldset>

      </form>
    </div>
  )
}