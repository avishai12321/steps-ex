import axios from 'axios';
const instance = axios.create({
    // baseURL: 'https://jsonplaceholder.typicode.com/comments'
});

export default instance; //create axios instance here in order to set the setting of the httpRequest in only one place.