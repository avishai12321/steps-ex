import './App.scss';
import MainPageContaier from './containers/mainPageContainer/MainPageContainer';
import Reviews from './components/reviews/Reviews'
import SideMenu from './components/sideMenu/SideMenu'

function App() {
  const mainElement = <Reviews></Reviews>
  const sideElement = <SideMenu></SideMenu>
  return (
    <MainPageContaier mainElement={mainElement} sideElement={sideElement}></MainPageContaier>
  );
}

export default App;
