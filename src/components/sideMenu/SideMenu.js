import './SideMenu.scss';
import SideMenuLink from '../sideMenuLink/SideMenuLink';
import profile from '../../assets/profile2.png';

export default function SideMenu() {
    return (
        <div className='sideMenuContainer'>
            <div className='avatarContainer'>
                <img id='profileAvatar' src={profile}></img>
            </div>
            <div className='linkContainer'>
                <SideMenuLink name='Reviews'></SideMenuLink>
                <SideMenuLink name='About Us'></SideMenuLink>
                <SideMenuLink name='Privacy'></SideMenuLink>
                <SideMenuLink name='Terms of Use'></SideMenuLink>
            </div>
        </div>
    )
}