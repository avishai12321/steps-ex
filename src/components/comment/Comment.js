import './Comment.scss';
import React from 'react';
import profile from '../../assets/profile.png'

export default function Comment(props) {
    const comment = props.value;
    return (
        <React.Fragment>{/*html container that allowed to be child of tr*/}
            <th>{comment.id}</th>
            <th>
                <div className='profileContainer'>
                    <img src={comment.img? comment.img:profile}></img>
                    <p>{comment.email}</p>
                </div>
            </th>
            <th>{comment.body}</th>
        </React.Fragment>
    )
}