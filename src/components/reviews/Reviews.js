import './Reviews.scss'
import axios from "../../third-party/axios";
import { useEffect, useState, } from 'react';
import Loading from '../loading/Loading';
import Comment from '../comment/Comment';
import InsertCommentPopUp from '../../containers/insertCommentPopUp/InsertCommentPopUp'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';

export default function Reviews() {

    //-----states
    const [isLoading, setLoading] = useState(true); //is the http still procceced.
    const [comments, setComments] = useState([]); //the comments the user requested.

    //-----componentDidMount
    useEffect(() => {
        getComments(20, 1);
    }, []);

    //fire when user scrolls.
    function handleOnScroll(e) {
        const target = e.target; //the div that contains the scrollbar
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight; //if scrollbar is at the bottom of the div.
        if (bottom) {
          loadMoreCommets();  
        }
    }

    //this function loads 20 new comments.
    function loadMoreCommets() {
        getComments(20, comments.length + 1);
    }
    //params: 
    //amount: (int) number of new comments you would like to get.
    //commentIndex: (int) the index of the first new commend you would like to get.

    //return: (sting) url query that about to be added to the baseUrl 

    function getComments(amount, commentIndex) {
        function urlGeneration() {
            let baseUrl = '';
            //loop form the first new comment to the last one you want to get (depends on 'amount')
            for (let index = commentIndex; index < commentIndex + amount; index++) {
                baseUrl += `&id=${index}`;
            }
            return baseUrl;
        }

        if (!isLoading) {
            setLoading(true)
        };
        let generatedUrl = urlGeneration();
        axios.get('https://jsonplaceholder.typicode.com/comments?' + generatedUrl) //add query ascci to the base url befor the query params. 
            .then(res => {
                const newComments = res.data;
                setComments(comments.concat(newComments));
                setLoading(false);
            })
    }

    return (
        <div className='ReviewsContainer'>
            <div className='header'>
                <div className='title'>Steps Reviews</div>
                <div className='addComment'>
                    <Popup trigger={<button className="button" id='topButton'> add comment</button>} modal>
                        {close => (
                            <InsertCommentPopUp close={close}></InsertCommentPopUp>
                        )}
                    </Popup>
                </div>
            </div>
            <div className='reviewTable' onScroll={handleOnScroll}>
                {isLoading ? <Loading></Loading> : null}
                <table>
                    <tbody>
                        <tr className='top'>
                            <th>
                                id
                            </th>
                            <th>
                                reviewr
                            </th>
                            <th>
                                review
                            </th>
                        </tr>
                        {comments.map((comment) => {
                            return (
                                <tr key={comment.id}>
                                    <Comment value={comment}></Comment>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
            <div className='footer'>
                <button className='button' onClick={loadMoreCommets}>load comments</button>
            </div>
        </div>
    );
}