import './SideMenuLink.scss'

export default function SideMenuLink(props) {
    return (
        <a>{props.name}</a>
    )
}